javascript: (function () {
    

    /* Ahrefs scraper bookmarklet by Aira Innovations */

    /* For use with the Industry Insights dashboard and GSheet - details at https://sites.google.com/aira.net/wiki/training-and-resources/ahrefs-scraper */

    /* Functions to send tracking hits */

    function returnMaintainerEmail() {
        return "tools@aira.net";
    };

    function genClientId() {
        /*Function which emulates GA4MP library for consistency https://github.com/adswerve/GA4-Measurement-Protocol-Python/blob/9576a870973712dea560553bbe59552ff04472d5/ga4mp/ga4mp.py:
        
        [Utility function for generating a new client ID matching the typical format of 10 random digits and the UNIX timestamp in seconds, joined by a period.]

        This is called by sendHit()

        */

        /* Get random string of digits */
        let randomArray = Array.from({
            length: 10
        }, () => Math.floor(Math.random() * 10));
        let randomString = randomArray.join("");

        /* Get current UNIX timestamp in seconds */
        let unixTime = Date.now();

        /* Cut down to ten digits as per other client id generators */
        unixTime = String(unixTime).substring(0, 10);

        /* Return both with decimal point */
        let result = randomString + "." + unixTime;
        return result;
    };

    function handleTracking(
        parameters = {},
        pageTitle = undefined,
        pageLocation = undefined,
        eventName = "pageview",
        stage = "unknown",
        callingFunction = undefined,
        clientId = undefined,
        testing = false,
        ...arguments
    ) {
        /*
    Function to handle sending an analytics hit to GA4

    Parameters;

    - parameters (dictionary -
    BE CAREFUL not to add a silly number to avoid flooding our analytics)

    - pageTitle (string - default is None)

    - pageLocation (string - default is None)

    - eventName (string - default is "pageview" | used to show events as hits to pages)

    - stage (string - default is "unknown" | used to categorise which point we're at in the script running)

    - clientId (string - default is "undefined" | used to join up tracking hits into one "session")

    - callingFunction (string - default is "undefined" | used to categorise which script is sending a hit if not defined then will be extracted by this function)


    *Naming conventions*
    - pageTitle and page_location should include just the script name and it should be consistent within each script
    i.e. when running in keyword_scrape script - set the page_location to "keyword_scrape" at the start and just
    reference that each time you send a tracking hit (this is for consistency and ease of report pivoting)
    - pageLocation and page_title should be lowercase
    - pageLocation should have underscores instead of spaces
    - pageTitle should have spaces where needed
    - callingFunction should match the function name - word breaks should be underscores_in_python and camelCaseInJavaScript


 */


        /* Getting the name of the function which called this one thanks to
           https://stackoverflow.com/questions/13432501/enforcing-strict-mode-in-google-apps-script-files-using-chrome-on-chromebook

        */

        /* Get calling function
         ===================================================
         
         */

        if (callingFunction == undefined) {
            /* Try to extract function but if fail - fall back to "unknown" */

            try {
                callingFunction = stack2lines_((new Error()).stack);
            } catch {
                callingFunction = "unknown";
            };
        };

        /* End of get calling function
        ^ ^  ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^
        */

        try {
            /* Send hit
             ===================================================
             */

            let dictToSend = {};

            /* Check the parameters we want to send in case we're sending a silly number */
            let objKeys = Object.keys(parameters);

            if (objKeys.length > 10) {
                /* Send an error and then just take a selection of the parameters */
                sendTrackingError("Too many parameters", callingFunction, [parameters]);

                let keyNum = 0;

                for (const [key, value] of Object.entries(object)) {
                    keyNum++;
                    if (keyNum > 9) {
                        break;
                    } else {
                        dictToSend[key] = value;
                    }
                };
            } else {
                dictToSend = parameters;
            };

            /* Add in key information we include with all hits */
            dictToSend["page_location"] = pageLocation;
            dictToSend["page_title"] = pageTitle;
            dictToSend["stage"] = stage;

            /* Add in consultant email if we can */
            try {
                var userName = getUser();
                if (userName != null) {
                    dictToSend["consultant"] = userName;
                };
            } catch {
                'Do nothing';
            };

            clientId = genClientId();

            /* Can't get response code because of CORS */
            sendHit(clientId, eventName, dictToSend, testing);


            /* End of send hit
             ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
             */



            /* Error handling
             ===================================================
             */


        } catch (error) {

            /* Handle if the tracking hit fails for some reason - to avoid breaking the code */

            try {
                /* Shouldn't need this try/catch because of the error handling in the trackingError function 
                 but this is belt-and-braces */

                sendTrackingError(error, callingFunction, [parameters]);
            } catch (errTwo) {
                console.log("Failed to send error message: " + errTwo);
            };
        };

        /* End of error handling
         ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
         */

        return clientId; /* don't want to accidentally overwrite clientId with undefined just because there was an error */


    };

    async function sendHit(
        clientId,
        eventName,
        dictToSend,
        testing) {

        console.log("Sending hit");

        /* Construct what we're sending */
        let urlToFetch = `https://europe-west2-project-aira-gsc-pipeline.cloudfunctions.net/send-ga-mp-hit`;
        /*
           We can't send direct to GA4 so we have to package everything up and send it to a cloud function API which will forward on
           This is because GA4 shadowblocks AppScript IP addresses
           For some reason this is breaking the hit tracking
           if (clientId!=undefined){
             dictToSend[clientId] = clientId
             } */

        dictToSend["event_name"] = eventName;
        dictToSend["message"] = "test";
        dictToSend["client_id"] = clientId;

        /* Structure request thanks to https://stackoverflow.com/questions/17351937/using-google-apps-script-to-post-json-data */
        let payload = JSON.stringify(dictToSend);


        var headers = {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Authorization": "Basic _authcode_"
        };


        /* Send and record response */
        try {
      

            var whatImSending = {
                method: "POST",
                headers: headers,
                body: payload,
                mode: 'no-cors'
            };
            console.log("Sending: ");
            console.log(whatImSending);

    const response = await fetch(urlToFetch, whatImSending);

  } catch (error) {
    console.error("There was a problem with the fetch operation:", error);
        };
    };
  
  
  function stack2lines_(stack) {
  /*Hacky function to extract the "calling function" when we are sending a hit
  
  Parameters;
  
  - stack (string)
  
  Example stack;

  "Error
    at sendHit (Send hit:45:29)
    at myFunction (Send hit:130:3)
    at __GS_INTERNAL_top_function_call__.gs:1:8"
  
  
  */

  /* Consistently includes the word "at" before each function
   so splitting on that */
      let splitStack = stack.split("at");
  
  /* The first block should be 'Error' the second block (after the first "at") 
   is sendHit, the block after THAT one should be the function that called it
   because of 0 indexing we extract with [2] */
      let callingFunction = splitStack[2].trim(); /* Get rid of whitespace */

  
  let rx = /([^\(\s]+)/g; /* Extract anything that isn't a bracket or a space */
  /* Check the extracted array and take first element */
  let fNameArray = rx.exec(callingFunction);
  let fName = fNameArray[0];

  return fName;
};
  
  
  function sendTrackingError(error, callingFunction, parameters){
    /* For the time being just dummy to make sure all this works */
      console.log("ERROR");
      console.log(error);
  };
  
  
  function getUser(){
  /* For the time being just dummy to make sure all this works */ 
  return userName;
};

    var scriptName = "sales_ahrefs_scraper";
    var clientId = genClientId();


    /* End functions to send tracking hits */

    try {

        handleTracking({
      "stage": "start"
    }, scriptName, scriptName, "pageview", "start");

    function getElementsByInnerText(text, elemType, searchSpace = document) {
        const matches = []; /*Loop through all the elements of a certain type*/
        for (const elem of searchSpace.querySelectorAll(elemType)) {
            /*If any of the matching elements match our text then we put them in our match list*/
            if (elem.textContent === text) {
                matches.push(elem);
            };
        };
        console.log(matches); /*Return the list of matches*/
        return matches;
    }; /* Utils thanks to https://stackoverflow.com/questions/74275776/doing-a-screen-capture-in-chrome-of-all-divs-under-element-using-getdisplaymedia */
    async function drawToCanvas(stream, filename) {
        const canvas = document.createElement("canvas");
        const video = document.createElement("video");
        video.srcObject = stream; /*Play it.*/
        await video.play(); /*Draw one video frame to canvas.*/
        canvas.width = video.videoWidth;
        canvas.height = video.videoHeight;
        canvas.getContext("2d").drawImage(video, 0, 0);
        canvas.toBlob((blob) => {
            saveFile(blob, filename + "_traffic.jpeg");
        }); /* End screen sharing thanks to https://stackoverflow.com/questions/58327561/how-to-manually-stop-getdisplaymedia-stream-to-end-screen-capture */
        let [track] = stream.getVideoTracks();
        track.stop();
        return canvas;
    };
    const saveFile = async (blob, suggestedName) => {
        /* Thanks to https://web.dev/patterns/files/save-a-file/             */
        /* Feature detection. The API needs to be supported        and the app not run in an iframe. */
        const supportsFileSystemAccess = 'showSaveFilePicker' in window && (() => {
            try {
                return window.self === window.top;
            } catch {
                return false;
            };
        })(); /* If the File System Access API is supported… */
        if (supportsFileSystemAccess) {
            try {
                /* Show the file save dialog. */
                const handle = await showSaveFilePicker({
                    suggestedName,
                }); /* Write the blob to the file. */
                const writable = await handle.createWritable();
                await writable.write(blob);
                await writable.close();
                return;
            } catch (err) {
                /* Fail silently if the user has simply canceled the dialog. */
                if (err.name !== 'AbortError') {
                    console.error(err.name, err.message);
                    return;
                }
            }
        } /* Fallback if the File System Access API is not supported… */ /* Create the blob URL. */
        const blobURL = URL.createObjectURL(blob); /* Create the `<a download>` element and append it invisibly. */
        const a = document.createElement('a');
        a.href = blobURL;
        a.download = suggestedName;
        a.style.display = 'none';
        document.body.append(a); /* Programmatically click the element. */
        a.click(); /*  Revoke the blob URL and remove the element */
        setTimeout(() => {
            URL.revokeObjectURL(blobURL);
            a.remove();
        }, 1000);
    };
    async function getCropTarget(domainName) {
        let mainContentArea = document.querySelector(".css-11am06z-wrapperWithTopBorder");
        mainContentArea.scrollIntoView(true);
        let cropTarget = await CropTarget.fromElement(mainContentArea);
        let stream = await navigator.mediaDevices.getDisplayMedia({
            preferCurrentTab: true
        });
        let [track] = stream.getVideoTracks(); /* Start cropping the self-capture video track using the CropTarget <-- Magic! */
        await track.cropTo(cropTarget);
        let cleanedDomain = createDomainName(domainName);
        let canvas = drawToCanvas(stream, cleanedDomain); /* Removed canvas.toblob here because having to make function async meant this was called too soon */
    };

    function createDomainName(domainName) {
        return domainName.replace("/", "").replace(".", "_");
    } /*End Utils */
    console.log("STARTING");
    let clientName = document.querySelector("h2 > a").innerText;
    let topPanel = document.querySelector(".css-kb91qb-wrapper");
    let or_search = document.querySelector(".css-ly8xs8-groupOrganicSearch");
    let paid_search = document.querySelector(".css-j0rdph-groupPaidSearch");
    let dr = getElementsByInnerText("DR", "div", topPanel)[0].parentElement.children[1].children[0].innerText;
    let backlinks = getElementsByInnerText("Backlinks", "div", topPanel)[0].parentElement.children[1].children[0].innerText;
    let ref_domains = getElementsByInnerText("Ref. domains", "div", topPanel)[0].parentElement.children[1].children[0].innerText;
    let or_keywords = getElementsByInnerText("Keywords", "div", or_search)[0].parentElement.children[1].children[0].innerText;
    let or_traffic = getElementsByInnerText("Traffic", "div", or_search)[0].parentElement.children[1].children[0].innerText;
    let or_traffic_val = "0";
    let or_traffic_val_location = getElementsByInnerText("Value", "div", or_search)[0].parentElement.children[1].children[0];
    if (or_traffic_val_location.innerText != 'N/A') {
        or_traffic_val = getElementsByInnerText("Value", "div", or_search)[0].parentElement.children[1].children[0].children[0].children[0].innerText;
    };
    let paid_keywords = getElementsByInnerText("Keywords", "div", paid_search)[0].parentElement.children[1].children[0].innerText;
    let paid_traffic = getElementsByInnerText("Traffic", "div", paid_search)[0].parentElement.children[1].children[0].innerText;
    let paid_traffic_val = "0";
    let paid_traffic_val_location = getElementsByInnerText("Cost", "div", paid_search)[0].parentElement.children[1].children[0];
    if (paid_traffic_val_location.innerText != 'N/A') {
        paid_traffic_val = getElementsByInnerText("Cost", "div", paid_search)[0].parentElement.children[1].children[0].children[0].children[0].innerText;
    };
    let resArray = [clientName, backlinks, ref_domains, or_keywords, or_traffic, or_traffic_val, dr, " ", clientName, paid_keywords, paid_traffic, paid_traffic_val];
    let finalArray = [];
    for (i in resArray) {
        let elem = resArray[i];
        if (elem.includes("\n")) {
            elem = elem.split(/\n/)[0];
        };
        finalArray.push(elem);
    };
    let outputText = finalArray.join("\n");
    navigator.clipboard.writeText(outputText);
    null;
    getCropTarget(clientName);
    console.log("COMPLETE");
    
    
    handleTracking({
      "stage": "end"
    }, scriptName, scriptName, "pageview", "end");
    
    }
    catch (err) {
        
        handleTracking({
      "stage": "error"
    }, scriptName, scriptName, "pageview", "error");

    /* Throw the error to stop script execution */
    throw err;
    };
})();