# Ahrefs scraper bookmarklet

Use this to pull data from Ahrefs.


## Instructions

1. Select and copy **ALL** of the code [in this file linked here](ahrefs-scraper.js?plain=1)<br>You can easily copy the file contents by clicking the clipboard logo in the top-right corner.<br>![img](resources/copy-file-contents-button.png)

2. In your browser of choice (Google Chrome is recommended) right-click on your bookmarks bar and click "add page".<br>![img](resources/add-new-bookmark-to-bookmark-bar.png)

3. In the new bookmark window - give it a descriptive name like "Ahrefs Scraper" and in the URL field - paste in all of the code you just copied from that file.<br>![img](resources/paste-code-into-bookmark.png)

4. Go to [the Aira wiki page](https://sites.google.com/aira.net/wiki/training-and-resources/ahrefs-scraper) for instructions on how to use the Ahrefs scraper and the sheet and Looker Studio dashboard which visualise the data for you.


## Versions

### 1.0.0
- Extract data from Ahrefs and put in paste-able format
- Automatically save an image of the graphs on the page

### 1.1.0
- Add GA tool usage tracking
- Update documentation